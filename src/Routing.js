import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { NotFound, Error } from './views/Errors/Errors'
import D3 from './views/D3/D3'
import C3 from './views/C3/C3'
import HighchartsView from './views/Highcharts/Highcharts'
import Plotly from './views/Plotly/Plotly'
import Recharts from './views/Recharts/Recharts'
import ReactVis from './views/ReactVis/ReactVis'
import Semiotic from './views/Semiotic/Semiotic'

const Routing = () =>
  <Switch>
    <Route exact path='/' component={D3} />
    <Route exact path='/c3' component={C3} />
    <Route exact path='/highcharts' component={HighchartsView} />
    <Route exact path='/plotly' component={Plotly} />
    <Route exact path='/recharts' component={Recharts} />
    <Route exact path='/react-vis' component={ReactVis} />
    <Route exact path='/semiotic' component={Semiotic} />
    <Route exact path='/error' component={Error} />
    <Route component={NotFound} />
  </Switch>

export default Routing
