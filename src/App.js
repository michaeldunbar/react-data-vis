import React, { Component } from 'react'
import Routing from './Routing'
import routes from './routes'
import Navigation from './components/Navigation/Navigation'
import './App.scss'

class App extends Component {
  render () {
    return (
      <article className='app'>
        <a href='#content' className='skip-link'>Skip to main content</a>
        <header>
          <Navigation routes={routes} />
        </header>
        <main>
          <Routing />
        </main>
      </article>
    )
  }
}

export default App
