import React from 'react'
import { shallow } from 'enzyme'

import Navigation from './Navigation'

describe('App', () => {
  it('should render without crashing', () => {
    shallow(<Navigation routes={[]} />)
  })
})
