import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import './Navigation.scss'

const Navigation = ({ routes }) =>
  <nav className='navigation'>
    <ul className='navigation-list'>
      { routes.map(route =>
        <li key={route.url} className='navigation-item'>
          <NavLink exact to={route.url} className='navigation-link' activeClassName='active'>{route.name}</NavLink>
        </li>)
      }
    </ul>
  </nav>

Navigation.propTypes = {
  routes: PropTypes.array
}

export default Navigation
