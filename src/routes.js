const routes = [
  {
    name: 'D3',
    url: '/'
  },
  {
    name: 'C3',
    url: '/c3'
  },
  {
    name: 'Highcharts',
    url: '/highcharts'
  },
  {
    name: 'Plotly.js',
    url: '/plotly'
  },
  {
    name: 'Recharts',
    url: '/recharts'
  },
  {
    name: 'React Vis',
    url: '/react-vis'
  },
  {
    name: 'Semiotic',
    url: '/semiotic'
  }
]

export default routes
