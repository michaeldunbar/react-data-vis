import React from 'react'
import PlotlyBarChart from './PlotlyBarChart'
import './Plotly.scss'

const options = {
  mode: 'group',
  height: 300,
  title: 'Number of fruit picked'
}

const data = [
  {
    name: 'Apples',
    type: 'bar',
    x: ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
    y: [28, 36, 84, 180, 240, 22]
  },
  {
    name: 'Oranges',
    type: 'bar',
    x: ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
    y: [14, 25, 124, 196, 210, 17]
  },
  {
    name: 'Pears',
    type: 'bar',
    x: ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
    y: [22, 42, 64, 91, 110, 46]
  }
]

const Plotly = () =>
  <section id='content' className='content'>
    <div className='summary'>
      <h1><a href='https://plot.ly/javascript' target='_blank' rel='noopener noreferrer'>Plotly.js</a></h1>
      <p>Built on top of d3.js and stack.gl, plotly.js is a high-level, declarative charting library.</p>
    </div>
    <div className='example'>
      <h3>A simple bar chart</h3>
      <PlotlyBarChart data={data} options={options} />
      <a href='https://bitbucket.org/michaeldunbar/react-data-vis/src/master/src/views/Plotly/PlotlyBarChart.js' target='_blank' rel='noopener noreferrer' className='button'>
        <i className='fas fa-code' />Sample code
      </a>
    </div>
    <div className='review'>
      <div>
        <h2>Pros</h2>
        <ul>
          <li>Open source</li>
          <li>JavaScript framework agnostic</li>
          <li>Large, active community and good documentation</li>
          <li>Good selection of charts and very customisable</li>
        </ul>
      </div>
      <div>
        <h2>Cons</h2>
        <ul>
          <li>Based on DOM manipulation</li>
          <li>Responsive support seems bolted on and not great</li>
          <li>Lots of options that need to be configured in a very specific format. Get ready to RTFM.</li>
        </ul>
      </div>
    </div>
    <div className='recommendation'>
      <h2 className='lowercase'>Recommendation &amp; comments</h2>
      <p>A good open source alternative to Highcharts, if you can't afford the license. Not as feature rich as Highcharts and was definitely not designed mobile first.</p>
      <p>It requires the React wrapper library to disguise the DOM manipulation happening under the hood, but with this it works well with React principles.</p>
      <p>I also found that Plotly would not build or run tests in my React app without reconfiguring webpack, which I didn't want to do. Resorted to old skool CDN link.</p>
    </div>
  </section>

export default Plotly
