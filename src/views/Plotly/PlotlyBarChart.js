/* global Plotly:true */

import React from 'react'
import createPlotlyComponent from 'react-plotly.js/factory'
import PropTypes from 'prop-types'

// View parent component to see how data is configured

const Plot = createPlotlyComponent(Plotly)
const PlotlyBarChart = ({ data, options }) =>
  <Plot
    data={data}
    layout={{ barmode: options.mode, height: options.height, title: options.title, responsive: true }}
  />

PlotlyBarChart.propTypes = {
  data: PropTypes.array,
  options: PropTypes.object
}

export default PlotlyBarChart
