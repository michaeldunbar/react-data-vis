import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { scaleLinear } from 'd3-scale'
import { max } from 'd3-array'
import { select } from 'd3-selection'

class D3BarChart extends Component {
  constructor (props) {
    super(props)
    this.createBarChart = this.createBarChart.bind(this)
  }

  componentDidMount () {
    this.createBarChart()
  }

  createBarChart () {
    const { data, options } = this.props
    const node = this.node
    const dataMax = max(data)
    const yScale = scaleLinear()
      .domain([0, dataMax])
      .range([0, options.height])

    select(node)
      .selectAll('rect')
      .data(data)
      .enter()
      .append('rect')

    select(node)
      .selectAll('rect')
      .data(data)
      .exit()
      .remove()

    select(node)
      .selectAll('rect')
      .data(data)
      .style('fill', '#fe9922')
      .style('stroke', '#fff')
      .attr('x', (d, i) => i * 30)
      .attr('y', d => options.height - yScale(d))
      .attr('height', d => yScale(d))
      .attr('width', 30)
  }

  render () {
    const { options } = this.props
    return <svg ref={(node) => { this.node = node }} height={options.height} />
  }
}

D3BarChart.propTypes = {
  data: PropTypes.array,
  options: PropTypes.object
}

export default D3BarChart
