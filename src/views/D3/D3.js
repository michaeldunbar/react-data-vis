import React from 'react'
import D3BarChart from './D3BarChart'

const D3 = () =>
  <section id='content' className='content'>
    <div className='summary'>
      <h1><a href='https://d3js.org' target='_blank' rel='noopener noreferrer'>D3.js</a></h1>
      <p>D3.js is a JavaScript library for manipulating documents based on data. It is <strong>not</strong> a charting library like the others.</p>
    </div>
    <div className='example'>
      <h3>A simple bar chart</h3>
      <D3BarChart data={[5, 10, 1, 3]} options={{ height: 200 }} />
      <a href='https://bitbucket.org/michaeldunbar/react-data-vis/src/master/src/views/D3/D3BarChart.js' target='_blank' rel='noopener noreferrer' className='button'>
        <i className='fas fa-code' />Sample code
      </a>
    </div>
    <div className='review'>
      <div>
        <h2>Pros</h2>
        <ul>
          <li>Open source</li>
          <li>JavaScript framework agnostic</li>
          <li>Large active community</li>
          <li>You can literally visualise anything you can imagine.</li>
        </ul>
      </div>
      <div>
        <h2>Cons</h2>
        <ul>
          <li>Steep learning curve</li>
          <li>DOM manipulation to render</li>
          <li>Verbose code to create simple visualisations.</li>
        </ul>
      </div>
    </div>
    <div className='recommendation'>
      <h2 className='lowercase'>Recommendation &amp; comments</h2>
      <p>Use for bespoke visualisations, not basic charts.</p>
      <p>You can do a lot with this library, but it takes an equal amount of time and code in my experience.</p>
    </div>
  </section>

export default D3
