import React from 'react'

const NotFound = () =>
  <section id='content' className='content error'>
    <h1><i className='far fa-meh' /> Page not found</h1>
    <p>We're sorry, but the page you have requested has either been removed or permanently moved.</p>
  </section>

const Error = () =>
  <section id='content' className='content error'>
    <h1><i className='far fa-frown' /> Well... this is embarrassing!</h1>
    <p>We're sorry, but an error seems to have occurred. If the problem persists, please let us know.</p>
  </section>

export { NotFound, Error }
