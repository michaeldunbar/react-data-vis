import React from 'react'
import { shallow } from 'enzyme'

import { NotFound, Error } from './Errors'

describe('Errors', () => {
  it('should render Not Found view without crashing', () => {
    shallow(<NotFound />)
  })

  it('should render an Error view without crashing', () => {
    shallow(<Error />)
  })
})
