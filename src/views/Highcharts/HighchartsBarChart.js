import React from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import PropTypes from 'prop-types'

// View parent component to see how data is configured

const createBarChart = (data) => {
  const options = {
    chart: {
      type: 'column'
    },
    title: {
      text: data.title
    },
    xAxis: {
      categories: data.categories
    },
    yAxis: {
      title: {
        text: data.yAxis.title
      }
    },
    series: data.series
  }

  return <HighchartsReact highcharts={Highcharts} options={options} />
}

const HighChartsBarChart = ({ data }) => createBarChart(data)

HighChartsBarChart.propTypes = {
  data: PropTypes.object
}

export default HighChartsBarChart
