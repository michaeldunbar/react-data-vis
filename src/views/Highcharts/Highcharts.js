import React from 'react'
import HighChartsBarChart from './HighchartsBarChart'

const data = {
  title: 'Number of fruit picked',
  categories: ['Apr', 'May', 'Jun', 'July', 'Aug', 'Sep'],
  yAxis: {
    title: 'Number of fruit'
  },
  series: [
    { name: 'Apples', data: [28, 36, 84, 180, 240, 22] },
    { name: 'Oranges', data: [14, 25, 124, 196, 210, 17] },
    { name: 'Pears', data: [22, 42, 64, 91, 110, 46] }
  ]
}

const HighchartsView = () =>
  <section id='content' className='content'>
    <div className='summary'>
      <h1><a href='https://www.highcharts.com/demo' target='_blank' rel='noopener noreferrer'>Highcharts</a></h1>
      <p>Highcharts is quite possibly the largest JavaScript charting library available.</p>
    </div>
    <div className='example'>
      <h3>A simple bar chart</h3>
      <HighChartsBarChart data={data} />
      <a href='https://bitbucket.org/michaeldunbar/react-data-vis/src/master/src/views/Highcharts/HighchartsBarChart.js' target='_blank' rel='noopener noreferrer' className='button'>
        <i className='fas fa-code' />Sample code
      </a>
    </div>
    <div className='review'>
      <div>
        <h2>Pros</h2>
        <ul>
          <li>JavaScript framework agnostic</li>
          <li>Packed with chart types and features</li>
          <li>Large, active community and excellent documentation</li>
          <li>Actively developed by in-house team</li>
          <li>Leading the way in accessible charts</li>
          <li>Very customisable.</li>
        </ul>
      </div>
      <div>
        <h2>Cons</h2>
        <ul>
          <li>Propietary software</li>
          <li>Old library based on DOM manipulation</li>
          <li>Lots of options that need to be configured in a very specific format. Get ready to RTFM.</li>
        </ul>
      </div>
    </div>
    <div className='recommendation'>
      <h2 className='lowercase'>Recommendation &amp; comments</h2>
      <p>If you can afford to use this, you'd need a good reason not to.</p>
      <p>It requires the React wrapper library to disguise the DOM manipulation happening under the hood, but with this it works well with React principles.</p>
    </div>
  </section>

export default HighchartsView
