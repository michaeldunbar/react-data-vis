import React from 'react'
import ReactVisBarChart from './ReactVisBarChart'

const data = [
  [ { x: 'Apples', y: 28 }, { x: 'Oranges', y: 14 }, { x: 'Pears', y: 22 } ],
  [ { x: 'Apples', y: 36 }, { x: 'Oranges', y: 25 }, { x: 'Pears', y: 42 } ],
  [ { x: 'Apples', y: 84 }, { x: 'Oranges', y: 124 }, { x: 'Pears', y: 64 } ],
  [ { x: 'Apples', y: 180 }, { x: 'Oranges', y: 196 }, { x: 'Pears', y: 91 } ],
  [ { x: 'Apples', y: 240 }, { x: 'Oranges', y: 210 }, { x: 'Pears', y: 110 } ],
  [ { x: 'Apples', y: 22 }, { x: 'Oranges', y: 17 }, { x: 'Pears', y: 46 } ]
]

const options = {
  height: 300
}

const ReactVis = () =>
  <section id='content' className='content'>
    <div className='summary'>
      <h1><a href='https://uber.github.io/react-vis' target='_blank' rel='noopener noreferrer'>React-vis</a></h1>
      <p>React-vis is a React visualization library created by Uber.</p>
    </div>
    <div className='example'>
      <h3>A simple bar chart</h3>
      <ReactVisBarChart data={data} options={options} />
      <a href='https://bitbucket.org/michaeldunbar/react-data-vis/src/master/src/views/ReactVis/ReactVisBarChart.js' target='_blank' rel='noopener noreferrer' className='button'>
        <i className='fas fa-code' />Sample code
      </a>
    </div>
    <div className='review'>
      <div>
        <h2>Pros</h2>
        <ul>
          <li>Open source</li>
          <li>Designed for React</li>
          <li>Fair selection of charts with some configuration options.</li>
        </ul>
      </div>
      <div>
        <h2>Cons</h2>
        <ul>
          <li>Lacking documentation &amp; community activity</li>
          <li>Dependant on React</li>
          <li>It seemed to take quite some piecing together to get this simple responsive bar chart to display.</li>
        </ul>
      </div>
    </div>
    <div className='recommendation'>
      <h2 className='lowercase'>Recommendation &amp; comments</h2>
      <p>This could be a great charting library for React. I wouldn't recommend it yet though.</p>
      <p>The chart above is as far as I managed to get with a clustered bar chart in the time I had available, something that has been simple in other libraries.
        For me, the main issue with React-vis is also it's strength - it's flexibility. It is not opinionated and very flexible expecting you to piece together
        the components you want to create the chart you want. This would be a great approach if the documentation wasn't so lacking. As is, it is just difficult to work with.
      </p>
    </div>
  </section>

export default ReactVis
