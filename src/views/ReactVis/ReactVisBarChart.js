import React from 'react'
import { FlexibleWidthXYPlot, VerticalGridLines, HorizontalGridLines, XAxis, YAxis, VerticalBarSeries } from 'react-vis'
import PropTypes from 'prop-types'

// View parent component to see how data is configured

const ReactVisBarChart = ({ data, options }) =>
  <FlexibleWidthXYPlot height={options.height} xType='ordinal'>
    <VerticalGridLines />
    <HorizontalGridLines />
    <XAxis />
    <YAxis />
    {
      data.map((d, i) =>
        <VerticalBarSeries key={i} data={d} />)
    }
  </FlexibleWidthXYPlot>

ReactVisBarChart.propTypes = {
  data: PropTypes.array,
  options: PropTypes.object
}

export default ReactVisBarChart
