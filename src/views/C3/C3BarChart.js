/* global c3:true */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'c3/c3.css'

// View parent component to see how data is configured

class C3BarChart extends Component {
  constructor (props) {
    super(props)
    this.createBarChart = this.createBarChart.bind(this)
  }

  componentDidMount () {
    this.createBarChart()
  }

  createBarChart () {
    const { data, options } = this.props
    c3.generate({
      bindto: `#${options.id}`,
      data: {
        columns: data,
        type: 'bar'
      },
      axis: {
        x: {
          type: 'category',
          categories: options.categories
        }
      }
    })
  }

  render () {
    const { options } = this.props
    return (<div id={options.id} style={{ height: options.height }} />)
  }
}

C3BarChart.propTypes = {
  data: PropTypes.array
}

export default C3BarChart
