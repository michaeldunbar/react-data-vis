import React from 'react'
import C3BarChart from './C3BarChart'

const data = [
  ['Apples', 28, 36, 84, 180, 240, 22],
  ['Oranges', 14, 25, 124, 196, 210, 17],
  ['Pears', 22, 42, 64, 91, 110, 46]
]

const options = {
  id: 'bar',
  height: 300,
  categories: ['Apr', 'May', 'Jun', 'July', 'Aug', 'Sep']
}

const C3 = () =>
  <section id='content' className='content'>
    <div className='summary'>
      <h1><a href='https://c3js.org' target='_blank' rel='noopener noreferrer'>C3.js</a></h1>
      <p>C3.js is a D3 based reusable chart library. In other words, it is a wrapper for common charts built using D3.js.</p>
    </div>
    <div className='example'>
      <h3>A simple bar chart</h3>
      <C3BarChart data={data} options={options} />
      <a href='https://bitbucket.org/michaeldunbar/react-data-vis/src/master/src/views/C3/C3BarChart.js' target='_blank' rel='noopener noreferrer' className='button'>
        <i className='fas fa-code' />Sample code
      </a>
    </div>
    <div className='review'>
      <div>
        <h2>Pros</h2>
        <ul>
          <li>Open source</li>
          <li>JavaScript framework agnostic</li>
          <li>Active community and good documentation</li>
          <li>Built on the familiar D3.js library.</li>
        </ul>
      </div>
      <div>
        <h2>Cons</h2>
        <ul>
          <li>DOM manipulation to render</li>
          <li>Limited selection of chart types</li>
          <li>Requires D3.js as well as core library.</li>
        </ul>
      </div>
    </div>
    <div className='recommendation'>
      <h2 className='lowercase'>Recommendation &amp; comments</h2>
      <p>Use for projects with basic charting requirements.</p>
      <p>Used in React it feels like an old style library from the jQuery era made to fit.</p>
      <p>I also found that C3 would not build or run tests in my React app without reconfiguring webpack, which I didn't want to do. Resorted to old skool CDN links.</p>
    </div>
  </section>

export default C3
