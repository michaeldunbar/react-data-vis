import React from 'react'
import SemioticBarChart from './SemioticBarChart'
import './Semiotic.scss'

const data = [
  { month: 'Apr', type: 'Apples', value: 28 },
  { month: 'May', type: 'Apples', value: 36 },
  { month: 'Jun', type: 'Apples', value: 84 },
  { month: 'Jul', type: 'Apples', value: 180 },
  { month: 'Aug', type: 'Apples', value: 240 },
  { month: 'Sep', type: 'Apples', value: 22 },
  { month: 'Apr', type: 'Oranges', value: 14 },
  { month: 'May', type: 'Oranges', value: 25 },
  { month: 'Jun', type: 'Oranges', value: 124 },
  { month: 'Jul', type: 'Oranges', value: 196 },
  { month: 'Aug', type: 'Oranges', value: 210 },
  { month: 'Sep', type: 'Oranges', value: 17 },
  { month: 'Apr', type: 'Pears', value: 22 },
  { month: 'May', type: 'Pears', value: 42 },
  { month: 'Jun', type: 'Pears', value: 64 },
  { month: 'Jul', type: 'Pears', value: 91 },
  { month: 'Aug', type: 'Pears', value: 110 },
  { month: 'Sep', type: 'Pears', value: 46 }
]

const options = {
  title: 'Number of fruit',
  colours: {
    Apples: '#3977AF',
    Oranges: '#EF8536',
    Pears: '#519D3E'
  }
}

const Semiotic = () =>
  <section id='content' className='content'>
    <div className='summary'>
      <h1><a href='https://emeeks.github.io/semiotic/#/' target='_blank' rel='noopener noreferrer'>Semiotic</a></h1>
      <p>Semiotic is an opinionated data visualization framework for React. Like D3, it is not to be confused with a charting library although it can do that too.</p>
    </div>
    <div className='example'>
      <h3>A simple bar chart</h3>
      <SemioticBarChart data={data} options={options} />
      <a href='https://bitbucket.org/michaeldunbar/react-data-vis/src/master/src/views/Semiotic/SemioticBarChart.js' target='_blank' rel='noopener noreferrer' className='button'>
        <i className='fas fa-code' />Sample code
      </a>
    </div>
    <div className='review'>
      <div>
        <h2>Pros</h2>
        <ul>
          <li>Open source</li>
          <li>Designed for React</li>
          <li>Very customisable.</li>
        </ul>
      </div>
      <div>
        <h2>Cons</h2>
        <ul>
          <li>One-man project - Elijah Meeks of Netflix</li>
          <li>Small community and basic documentation</li>
          <li>Dependant on React.</li>
        </ul>
      </div>
    </div>
    <div className='recommendation'>
      <h2 className='lowercase'>Recommendation &amp; comments</h2>
      <p>I would seriously consider using this framework, especially for projects that require bespoke charts or visualisations.</p>
      <p>
        Semiotic is a brilliant data visualisation and bespoke chart framework that sits somewhere between D3.js and the chart libraries. For standard charts, I would use
        something else.
      </p>
    </div>
  </section>

export default Semiotic
