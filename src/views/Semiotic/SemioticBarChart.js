import React from 'react'
import { ResponsiveOrdinalFrame } from 'semiotic'
import PropTypes from 'prop-types'

// View parent component to see how data is configured

const SemioticBarChart = ({ data, options }) =>
  <ResponsiveOrdinalFrame
    size={[600, 300]}
    data={data}
    axis={{ label: options.title }}
    oAccessor={'month'}
    rAccessor={'value'}
    style={d => ({ fill: options.colours[d.type], stroke: '#fff' })}
    type={'bar'}
    oLabel
    responsiveWidth
    pieceHoverAnnotation
    tooltipContent={d =>
      <div className='tooltip-content'>
        <span>{d.data.type} {d.value}</span>
      </div>}
  />

SemioticBarChart.propTypes = {
  data: PropTypes.array,
  options: PropTypes.object
}

export default SemioticBarChart
