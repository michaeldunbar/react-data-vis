import React from 'react'
import RechartsBarChart from './RechartsBarChart'

const data = [
  { month: 'Apr', Apples: 28, Oranges: 14, Pears: 22 },
  { month: 'May', Apples: 36, Oranges: 25, Pears: 42 },
  { month: 'Jun', Apples: 84, Oranges: 124, Pears: 64 },
  { month: 'Jul', Apples: 180, Oranges: 196, Pears: 91 },
  { month: 'Aug', Apples: 240, Oranges: 210, Pears: 110 },
  { month: 'Sep', Apples: 22, Oranges: 17, Pears: 46 }
]

const options = {
  xAxis: 'month',
  bars: [
    { name: 'Apples', colour: '#3977AF' },
    { name: 'Oranges', colour: '#EF8536' },
    { name: 'Pears', colour: '#519D3E' }
  ]
}

const Recharts = () =>
  <section id='content' className='content'>
    <div className='summary'>
      <h1><a href='http://recharts.org' target='_blank' rel='noopener noreferrer'>Recharts</a></h1>
      <p>A composable charting library built on React components.</p>
    </div>
    <div className='example'>
      <h3>A simple bar chart</h3>
      <RechartsBarChart data={data} options={options} />
      <a href='https://bitbucket.org/michaeldunbar/react-data-vis/src/master/src/views/Recharts/RechartsBarChart.js' target='_blank' rel='noopener noreferrer' className='button'>
        <i className='fas fa-code' />Sample code
      </a>
    </div>
    <div className='review'>
      <div>
        <h2>Pros</h2>
        <ul>
          <li>Open source</li>
          <li>Designed for React</li>
          <li>Active community and good documentation</li>
          <li>Lots of options for configuring charts.</li>
        </ul>
      </div>
      <div>
        <h2>Cons</h2>
        <ul>
          <li>Limited number of chart types</li>
          <li>Dependant on React</li>
          <li>Customisation seems quite basic beyond configuration.</li>
        </ul>
      </div>
    </div>
    <div className='recommendation'>
      <h2 className='lowercase'>Recommendation &amp; comments</h2>
      <p>Feels like the equivalent of C3.js built for React. I would definitely consider it for a React project that needed basic charting.</p>
    </div>
  </section>

export default Recharts
