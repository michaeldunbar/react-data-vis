import React from 'react'
import { BarChart, Bar, XAxis, YAxis, Tooltip, Legend, CartesianGrid, ResponsiveContainer } from 'recharts'

// View parent component to see how data is configured

const RechartsBarChart = ({ data, options }) =>
  <ResponsiveContainer height={300}>
    <BarChart data={data}>
      <CartesianGrid strokeDasharray='3 3' />
      <XAxis dataKey={options.xAxis} />
      <YAxis />
      <Tooltip />
      <Legend />
      {
        options.bars.map(bar => <Bar key={bar.name} dataKey={bar.name} fill={bar.colour} />)
      }
    </BarChart>
  </ResponsiveContainer>

export default RechartsBarChart
